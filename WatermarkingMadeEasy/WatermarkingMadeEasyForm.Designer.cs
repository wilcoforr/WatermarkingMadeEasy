﻿namespace WatermarkingMadeEasy
{
    partial class FormMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormMain));
            this.TextBoxWatermark = new System.Windows.Forms.TextBox();
            this.ButtonApplyWatermarkText = new System.Windows.Forms.Button();
            this.ComboBoxTextLocation = new System.Windows.Forms.ComboBox();
            this.ButtonSaveImage = new System.Windows.Forms.Button();
            this.TextBoxProgramLog = new System.Windows.Forms.TextBox();
            this.ComboBoxFonts = new System.Windows.Forms.ComboBox();
            this.ComboBoxFontSize = new System.Windows.Forms.ComboBox();
            this.ButtonSelectImage = new System.Windows.Forms.Button();
            this.LabelWatermarkText = new System.Windows.Forms.Label();
            this.LabelFontSize = new System.Windows.Forms.Label();
            this.LabelTextLocation = new System.Windows.Forms.Label();
            this.LabelFont = new System.Windows.Forms.Label();
            this.LabelImageDimensions = new System.Windows.Forms.Label();
            this.GroupBoxImageInformation = new System.Windows.Forms.GroupBox();
            this.TextBoxFileName = new System.Windows.Forms.TextBox();
            this.LabelFileName = new System.Windows.Forms.Label();
            this.LabelProgramLog = new System.Windows.Forms.Label();
            this.PictureBox = new System.Windows.Forms.PictureBox();
            this.ButtonColorDialog = new System.Windows.Forms.Button();
            this.NumUpDownPaddingY = new System.Windows.Forms.NumericUpDown();
            this.NumUpDownPaddingX = new System.Windows.Forms.NumericUpDown();
            this.LabelPaddingX = new System.Windows.Forms.Label();
            this.LabelPaddingY = new System.Windows.Forms.Label();
            this.NumUpDownTextOpacity = new System.Windows.Forms.NumericUpDown();
            this.LabelOpacity = new System.Windows.Forms.Label();
            this.ButtonOpenImagesFolder = new System.Windows.Forms.Button();
            this.NumUpDownXOpacity = new System.Windows.Forms.NumericUpDown();
            this.LabelXOpacity = new System.Windows.Forms.Label();
            this.ButtonBatchWatermark = new System.Windows.Forms.Button();
            this.ToolTipForTextBoxFilename = new System.Windows.Forms.ToolTip(this.components);
            this.CheckBoxApplyXWatermark = new System.Windows.Forms.CheckBox();
            this.ButtonSelectedColor = new System.Windows.Forms.Button();
            this.NumUpDownXBrushThickness = new System.Windows.Forms.NumericUpDown();
            this.LabelXThickness = new System.Windows.Forms.Label();
            this.form1BindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.form1BindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.GroupBoxImageInformation.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumUpDownPaddingY)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumUpDownPaddingX)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumUpDownTextOpacity)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumUpDownXOpacity)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumUpDownXBrushThickness)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.form1BindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.form1BindingSource1)).BeginInit();
            this.SuspendLayout();
            // 
            // TextBoxWatermark
            // 
            this.TextBoxWatermark.Location = new System.Drawing.Point(897, 28);
            this.TextBoxWatermark.MaxLength = 30;
            this.TextBoxWatermark.Name = "TextBoxWatermark";
            this.TextBoxWatermark.Size = new System.Drawing.Size(250, 20);
            this.TextBoxWatermark.TabIndex = 2;
            this.TextBoxWatermark.Text = "watermark text";
            this.TextBoxWatermark.TextChanged += new System.EventHandler(this.TextBoxWatermark_TextChanged);
            // 
            // ButtonApplyWatermarkText
            // 
            this.ButtonApplyWatermarkText.Location = new System.Drawing.Point(1174, 68);
            this.ButtonApplyWatermarkText.Name = "ButtonApplyWatermarkText";
            this.ButtonApplyWatermarkText.Size = new System.Drawing.Size(118, 50);
            this.ButtonApplyWatermarkText.TabIndex = 12;
            this.ButtonApplyWatermarkText.Text = "Apply\r\nWatermark";
            this.ButtonApplyWatermarkText.UseVisualStyleBackColor = true;
            this.ButtonApplyWatermarkText.Click += new System.EventHandler(this.ButtonApplyWatermarkText_Click);
            // 
            // ComboBoxTextLocation
            // 
            this.ComboBoxTextLocation.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ComboBoxTextLocation.FormattingEnabled = true;
            this.ComboBoxTextLocation.Location = new System.Drawing.Point(1027, 82);
            this.ComboBoxTextLocation.Name = "ComboBoxTextLocation";
            this.ComboBoxTextLocation.Size = new System.Drawing.Size(120, 21);
            this.ComboBoxTextLocation.TabIndex = 4;
            this.ComboBoxTextLocation.SelectedIndexChanged += new System.EventHandler(this.ComboBoxTextLocation_SelectedIndexChanged);
            // 
            // ButtonSaveImage
            // 
            this.ButtonSaveImage.Location = new System.Drawing.Point(1174, 124);
            this.ButtonSaveImage.Name = "ButtonSaveImage";
            this.ButtonSaveImage.Size = new System.Drawing.Size(118, 50);
            this.ButtonSaveImage.TabIndex = 13;
            this.ButtonSaveImage.Text = "Save\r\nWatermarked Image";
            this.ButtonSaveImage.UseVisualStyleBackColor = true;
            this.ButtonSaveImage.Click += new System.EventHandler(this.ButtonSaveImage_Click);
            // 
            // TextBoxProgramLog
            // 
            this.TextBoxProgramLog.Location = new System.Drawing.Point(12, 537);
            this.TextBoxProgramLog.Multiline = true;
            this.TextBoxProgramLog.Name = "TextBoxProgramLog";
            this.TextBoxProgramLog.ReadOnly = true;
            this.TextBoxProgramLog.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.TextBoxProgramLog.Size = new System.Drawing.Size(870, 132);
            this.TextBoxProgramLog.TabIndex = 5;
            // 
            // ComboBoxFonts
            // 
            this.ComboBoxFonts.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ComboBoxFonts.FormattingEnabled = true;
            this.ComboBoxFonts.Location = new System.Drawing.Point(897, 125);
            this.ComboBoxFonts.MaxDropDownItems = 20;
            this.ComboBoxFonts.Name = "ComboBoxFonts";
            this.ComboBoxFonts.Size = new System.Drawing.Size(250, 21);
            this.ComboBoxFonts.TabIndex = 5;
            this.ComboBoxFonts.SelectedIndexChanged += new System.EventHandler(this.ComboBoxFonts_SelectedIndexChanged);
            // 
            // ComboBoxFontSize
            // 
            this.ComboBoxFontSize.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ComboBoxFontSize.FormattingEnabled = true;
            this.ComboBoxFontSize.Location = new System.Drawing.Point(897, 82);
            this.ComboBoxFontSize.MaxDropDownItems = 10;
            this.ComboBoxFontSize.Name = "ComboBoxFontSize";
            this.ComboBoxFontSize.Size = new System.Drawing.Size(120, 21);
            this.ComboBoxFontSize.TabIndex = 3;
            this.ComboBoxFontSize.SelectedIndexChanged += new System.EventHandler(this.ComboBoxFontSize_SelectedIndexChanged);
            // 
            // ButtonSelectImage
            // 
            this.ButtonSelectImage.Location = new System.Drawing.Point(1174, 12);
            this.ButtonSelectImage.Name = "ButtonSelectImage";
            this.ButtonSelectImage.Size = new System.Drawing.Size(118, 50);
            this.ButtonSelectImage.TabIndex = 1;
            this.ButtonSelectImage.Text = "Select\r\nan Image";
            this.ButtonSelectImage.UseVisualStyleBackColor = true;
            this.ButtonSelectImage.Click += new System.EventHandler(this.ButtonSelectImage_Click);
            // 
            // LabelWatermarkText
            // 
            this.LabelWatermarkText.AutoSize = true;
            this.LabelWatermarkText.Location = new System.Drawing.Point(894, 12);
            this.LabelWatermarkText.Name = "LabelWatermarkText";
            this.LabelWatermarkText.Size = new System.Drawing.Size(83, 13);
            this.LabelWatermarkText.TabIndex = 10;
            this.LabelWatermarkText.Text = "Watermark Text";
            // 
            // LabelFontSize
            // 
            this.LabelFontSize.AutoSize = true;
            this.LabelFontSize.Location = new System.Drawing.Point(894, 66);
            this.LabelFontSize.Name = "LabelFontSize";
            this.LabelFontSize.Size = new System.Drawing.Size(97, 13);
            this.LabelFontSize.TabIndex = 11;
            this.LabelFontSize.Text = "Font Size (in pixels)";
            // 
            // LabelTextLocation
            // 
            this.LabelTextLocation.AutoSize = true;
            this.LabelTextLocation.Location = new System.Drawing.Point(1024, 66);
            this.LabelTextLocation.Name = "LabelTextLocation";
            this.LabelTextLocation.Size = new System.Drawing.Size(72, 13);
            this.LabelTextLocation.TabIndex = 12;
            this.LabelTextLocation.Text = "Text Location";
            // 
            // LabelFont
            // 
            this.LabelFont.AutoSize = true;
            this.LabelFont.Location = new System.Drawing.Point(896, 110);
            this.LabelFont.Name = "LabelFont";
            this.LabelFont.Size = new System.Drawing.Size(28, 13);
            this.LabelFont.TabIndex = 13;
            this.LabelFont.Text = "Font";
            // 
            // LabelImageDimensions
            // 
            this.LabelImageDimensions.AutoSize = true;
            this.LabelImageDimensions.Location = new System.Drawing.Point(6, 44);
            this.LabelImageDimensions.Name = "LabelImageDimensions";
            this.LabelImageDimensions.Size = new System.Drawing.Size(67, 13);
            this.LabelImageDimensions.TabIndex = 15;
            this.LabelImageDimensions.Text = "Dimensions: ";
            // 
            // GroupBoxImageInformation
            // 
            this.GroupBoxImageInformation.Controls.Add(this.TextBoxFileName);
            this.GroupBoxImageInformation.Controls.Add(this.LabelFileName);
            this.GroupBoxImageInformation.Controls.Add(this.LabelImageDimensions);
            this.GroupBoxImageInformation.Location = new System.Drawing.Point(897, 537);
            this.GroupBoxImageInformation.Name = "GroupBoxImageInformation";
            this.GroupBoxImageInformation.Size = new System.Drawing.Size(395, 132);
            this.GroupBoxImageInformation.TabIndex = 17;
            this.GroupBoxImageInformation.TabStop = false;
            this.GroupBoxImageInformation.Text = "Image Information";
            // 
            // TextBoxFileName
            // 
            this.TextBoxFileName.Location = new System.Drawing.Point(69, 19);
            this.TextBoxFileName.Name = "TextBoxFileName";
            this.TextBoxFileName.ReadOnly = true;
            this.TextBoxFileName.Size = new System.Drawing.Size(320, 20);
            this.TextBoxFileName.TabIndex = 0;
            // 
            // LabelFileName
            // 
            this.LabelFileName.AutoSize = true;
            this.LabelFileName.Location = new System.Drawing.Point(6, 22);
            this.LabelFileName.Name = "LabelFileName";
            this.LabelFileName.Size = new System.Drawing.Size(57, 13);
            this.LabelFileName.TabIndex = 16;
            this.LabelFileName.Text = "File Name:";
            // 
            // LabelProgramLog
            // 
            this.LabelProgramLog.AutoSize = true;
            this.LabelProgramLog.Location = new System.Drawing.Point(13, 518);
            this.LabelProgramLog.Name = "LabelProgramLog";
            this.LabelProgramLog.Size = new System.Drawing.Size(67, 13);
            this.LabelProgramLog.TabIndex = 18;
            this.LabelProgramLog.Text = "Program Log";
            // 
            // PictureBox
            // 
            this.PictureBox.BackColor = System.Drawing.SystemColors.Control;
            this.PictureBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PictureBox.Location = new System.Drawing.Point(12, 12);
            this.PictureBox.Name = "PictureBox";
            this.PictureBox.Size = new System.Drawing.Size(870, 500);
            this.PictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.PictureBox.TabIndex = 0;
            this.PictureBox.TabStop = false;
            this.PictureBox.Paint += new System.Windows.Forms.PaintEventHandler(this.PictureBox_Paint);
            // 
            // ButtonColorDialog
            // 
            this.ButtonColorDialog.Location = new System.Drawing.Point(897, 165);
            this.ButtonColorDialog.Name = "ButtonColorDialog";
            this.ButtonColorDialog.Size = new System.Drawing.Size(100, 23);
            this.ButtonColorDialog.TabIndex = 8;
            this.ButtonColorDialog.Text = "Select a Color";
            this.ButtonColorDialog.UseVisualStyleBackColor = true;
            this.ButtonColorDialog.Click += new System.EventHandler(this.ButtonColorDialog_Click);
            // 
            // NumUpDownPaddingY
            // 
            this.NumUpDownPaddingY.Location = new System.Drawing.Point(1097, 231);
            this.NumUpDownPaddingY.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            -2147483648});
            this.NumUpDownPaddingY.Name = "NumUpDownPaddingY";
            this.NumUpDownPaddingY.Size = new System.Drawing.Size(58, 20);
            this.NumUpDownPaddingY.TabIndex = 11;
            this.NumUpDownPaddingY.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.NumUpDownPaddingY.ValueChanged += new System.EventHandler(this.NumUpDownPaddingY_ValueChanged);
            // 
            // NumUpDownPaddingX
            // 
            this.NumUpDownPaddingX.Location = new System.Drawing.Point(998, 231);
            this.NumUpDownPaddingX.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            -2147483648});
            this.NumUpDownPaddingX.Name = "NumUpDownPaddingX";
            this.NumUpDownPaddingX.Size = new System.Drawing.Size(58, 20);
            this.NumUpDownPaddingX.TabIndex = 10;
            this.NumUpDownPaddingX.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.NumUpDownPaddingX.ValueChanged += new System.EventHandler(this.NumUpDownPaddingX_ValueChanged);
            // 
            // LabelPaddingX
            // 
            this.LabelPaddingX.AutoSize = true;
            this.LabelPaddingX.Location = new System.Drawing.Point(995, 202);
            this.LabelPaddingX.Name = "LabelPaddingX";
            this.LabelPaddingX.Size = new System.Drawing.Size(56, 26);
            this.LabelPaddingX.TabIndex = 24;
            this.LabelPaddingX.Text = "Padding X\r\n(in pixels)";
            this.LabelPaddingX.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // LabelPaddingY
            // 
            this.LabelPaddingY.AutoSize = true;
            this.LabelPaddingY.Location = new System.Drawing.Point(1094, 202);
            this.LabelPaddingY.Name = "LabelPaddingY";
            this.LabelPaddingY.Size = new System.Drawing.Size(56, 26);
            this.LabelPaddingY.TabIndex = 25;
            this.LabelPaddingY.Text = "Padding Y\r\n(in pixels)";
            this.LabelPaddingY.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // NumUpDownTextOpacity
            // 
            this.NumUpDownTextOpacity.Location = new System.Drawing.Point(897, 231);
            this.NumUpDownTextOpacity.Name = "NumUpDownTextOpacity";
            this.NumUpDownTextOpacity.Size = new System.Drawing.Size(58, 20);
            this.NumUpDownTextOpacity.TabIndex = 27;
            this.NumUpDownTextOpacity.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.NumUpDownTextOpacity.Value = new decimal(new int[] {
            90,
            0,
            0,
            0});
            this.NumUpDownTextOpacity.ValueChanged += new System.EventHandler(this.NumUpDownOpacity_ValueChanged);
            // 
            // LabelOpacity
            // 
            this.LabelOpacity.AutoSize = true;
            this.LabelOpacity.Location = new System.Drawing.Point(896, 202);
            this.LabelOpacity.Name = "LabelOpacity";
            this.LabelOpacity.Size = new System.Drawing.Size(54, 26);
            this.LabelOpacity.TabIndex = 28;
            this.LabelOpacity.Text = "Text\r\nOpacity %";
            // 
            // ButtonOpenImagesFolder
            // 
            this.ButtonOpenImagesFolder.Location = new System.Drawing.Point(1174, 234);
            this.ButtonOpenImagesFolder.Name = "ButtonOpenImagesFolder";
            this.ButtonOpenImagesFolder.Size = new System.Drawing.Size(118, 50);
            this.ButtonOpenImagesFolder.TabIndex = 30;
            this.ButtonOpenImagesFolder.Text = "Open\r\nImages Folder";
            this.ButtonOpenImagesFolder.UseVisualStyleBackColor = true;
            this.ButtonOpenImagesFolder.Click += new System.EventHandler(this.ButtonOpenImagesFolder_Click);
            // 
            // NumUpDownXOpacity
            // 
            this.NumUpDownXOpacity.Location = new System.Drawing.Point(898, 282);
            this.NumUpDownXOpacity.Name = "NumUpDownXOpacity";
            this.NumUpDownXOpacity.Size = new System.Drawing.Size(59, 20);
            this.NumUpDownXOpacity.TabIndex = 31;
            this.NumUpDownXOpacity.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.NumUpDownXOpacity.Value = new decimal(new int[] {
            25,
            0,
            0,
            0});
            this.NumUpDownXOpacity.ValueChanged += new System.EventHandler(this.NumUpDownXOpacity_ValueChanged);
            // 
            // LabelXOpacity
            // 
            this.LabelXOpacity.AutoSize = true;
            this.LabelXOpacity.Location = new System.Drawing.Point(896, 266);
            this.LabelXOpacity.Name = "LabelXOpacity";
            this.LabelXOpacity.Size = new System.Drawing.Size(64, 13);
            this.LabelXOpacity.TabIndex = 32;
            this.LabelXOpacity.Text = "X Opacity %";
            // 
            // ButtonBatchWatermark
            // 
            this.ButtonBatchWatermark.Location = new System.Drawing.Point(1174, 180);
            this.ButtonBatchWatermark.Name = "ButtonBatchWatermark";
            this.ButtonBatchWatermark.Size = new System.Drawing.Size(118, 48);
            this.ButtonBatchWatermark.TabIndex = 33;
            this.ButtonBatchWatermark.Text = "Batch\r\nWatermark Images";
            this.ButtonBatchWatermark.UseVisualStyleBackColor = true;
            this.ButtonBatchWatermark.Click += new System.EventHandler(this.ButtonBatchWatermark_Click);
            // 
            // ToolTipForTextBoxFilename
            // 
            this.ToolTipForTextBoxFilename.AutomaticDelay = 200;
            // 
            // CheckBoxApplyXWatermark
            // 
            this.CheckBoxApplyXWatermark.AutoSize = true;
            this.CheckBoxApplyXWatermark.Location = new System.Drawing.Point(974, 281);
            this.CheckBoxApplyXWatermark.Name = "CheckBoxApplyXWatermark";
            this.CheckBoxApplyXWatermark.Size = new System.Drawing.Size(117, 17);
            this.CheckBoxApplyXWatermark.TabIndex = 34;
            this.CheckBoxApplyXWatermark.Text = "Apply X Watermark";
            this.CheckBoxApplyXWatermark.UseVisualStyleBackColor = true;
            this.CheckBoxApplyXWatermark.CheckedChanged += new System.EventHandler(this.CheckBoxApplyXWatermark_CheckedChanged);
            // 
            // ButtonSelectedColor
            // 
            this.ButtonSelectedColor.BackColor = System.Drawing.Color.White;
            this.ButtonSelectedColor.Enabled = false;
            this.ButtonSelectedColor.Location = new System.Drawing.Point(998, 165);
            this.ButtonSelectedColor.Name = "ButtonSelectedColor";
            this.ButtonSelectedColor.Size = new System.Drawing.Size(150, 23);
            this.ButtonSelectedColor.TabIndex = 35;
            this.ButtonSelectedColor.UseVisualStyleBackColor = false;
            // 
            // NumUpDownXBrushThickness
            // 
            this.NumUpDownXBrushThickness.Location = new System.Drawing.Point(1097, 280);
            this.NumUpDownXBrushThickness.Maximum = new decimal(new int[] {
            20,
            0,
            0,
            0});
            this.NumUpDownXBrushThickness.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.NumUpDownXBrushThickness.Name = "NumUpDownXBrushThickness";
            this.NumUpDownXBrushThickness.Size = new System.Drawing.Size(58, 20);
            this.NumUpDownXBrushThickness.TabIndex = 36;
            this.NumUpDownXBrushThickness.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.NumUpDownXBrushThickness.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.NumUpDownXBrushThickness.ValueChanged += new System.EventHandler(this.NumUpDownXBrushThickness_ValueChanged);
            // 
            // LabelXThickness
            // 
            this.LabelXThickness.AutoSize = true;
            this.LabelXThickness.Location = new System.Drawing.Point(1094, 266);
            this.LabelXThickness.Name = "LabelXThickness";
            this.LabelXThickness.Size = new System.Drawing.Size(66, 13);
            this.LabelXThickness.TabIndex = 37;
            this.LabelXThickness.Text = "X Thickness";
            // 
            // form1BindingSource
            // 
            this.form1BindingSource.DataSource = typeof(WatermarkingMadeEasy.FormMain);
            // 
            // form1BindingSource1
            // 
            this.form1BindingSource1.DataSource = typeof(WatermarkingMadeEasy.FormMain);
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.ClientSize = new System.Drawing.Size(1304, 681);
            this.Controls.Add(this.LabelXThickness);
            this.Controls.Add(this.NumUpDownXBrushThickness);
            this.Controls.Add(this.ButtonSelectedColor);
            this.Controls.Add(this.CheckBoxApplyXWatermark);
            this.Controls.Add(this.ButtonBatchWatermark);
            this.Controls.Add(this.LabelXOpacity);
            this.Controls.Add(this.NumUpDownXOpacity);
            this.Controls.Add(this.ButtonOpenImagesFolder);
            this.Controls.Add(this.LabelOpacity);
            this.Controls.Add(this.NumUpDownTextOpacity);
            this.Controls.Add(this.LabelPaddingY);
            this.Controls.Add(this.LabelPaddingX);
            this.Controls.Add(this.NumUpDownPaddingX);
            this.Controls.Add(this.NumUpDownPaddingY);
            this.Controls.Add(this.ButtonColorDialog);
            this.Controls.Add(this.LabelProgramLog);
            this.Controls.Add(this.LabelFont);
            this.Controls.Add(this.LabelTextLocation);
            this.Controls.Add(this.LabelFontSize);
            this.Controls.Add(this.LabelWatermarkText);
            this.Controls.Add(this.ButtonSelectImage);
            this.Controls.Add(this.ComboBoxFontSize);
            this.Controls.Add(this.ComboBoxFonts);
            this.Controls.Add(this.TextBoxProgramLog);
            this.Controls.Add(this.ButtonSaveImage);
            this.Controls.Add(this.ComboBoxTextLocation);
            this.Controls.Add(this.ButtonApplyWatermarkText);
            this.Controls.Add(this.TextBoxWatermark);
            this.Controls.Add(this.PictureBox);
            this.Controls.Add(this.GroupBoxImageInformation);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FormMain";
            this.Text = "Watermarking Made Easy";
            this.GroupBoxImageInformation.ResumeLayout(false);
            this.GroupBoxImageInformation.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumUpDownPaddingY)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumUpDownPaddingX)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumUpDownTextOpacity)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumUpDownXOpacity)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumUpDownXBrushThickness)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.form1BindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.form1BindingSource1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox PictureBox;
        private System.Windows.Forms.TextBox TextBoxWatermark;
        private System.Windows.Forms.Button ButtonApplyWatermarkText;
        private System.Windows.Forms.ComboBox ComboBoxTextLocation;
        private System.Windows.Forms.Button ButtonSaveImage;
        private System.Windows.Forms.TextBox TextBoxProgramLog;
        private System.Windows.Forms.ComboBox ComboBoxFonts;
        private System.Windows.Forms.BindingSource form1BindingSource;
        private System.Windows.Forms.BindingSource form1BindingSource1;
        private System.Windows.Forms.ComboBox ComboBoxFontSize;
        private System.Windows.Forms.Button ButtonSelectImage;
        private System.Windows.Forms.Label LabelWatermarkText;
        private System.Windows.Forms.Label LabelFontSize;
        private System.Windows.Forms.Label LabelTextLocation;
        private System.Windows.Forms.Label LabelFont;
        private System.Windows.Forms.Label LabelImageDimensions;
        private System.Windows.Forms.GroupBox GroupBoxImageInformation;
        private System.Windows.Forms.Label LabelFileName;
        private System.Windows.Forms.Label LabelProgramLog;
        private System.Windows.Forms.Button ButtonColorDialog;
        private System.Windows.Forms.NumericUpDown NumUpDownPaddingY;
        private System.Windows.Forms.NumericUpDown NumUpDownPaddingX;
        private System.Windows.Forms.Label LabelPaddingX;
        private System.Windows.Forms.Label LabelPaddingY;
        private System.Windows.Forms.TextBox TextBoxFileName;
        private System.Windows.Forms.NumericUpDown NumUpDownTextOpacity;
        private System.Windows.Forms.Label LabelOpacity;
        private System.Windows.Forms.Button ButtonOpenImagesFolder;
        private System.Windows.Forms.NumericUpDown NumUpDownXOpacity;
        private System.Windows.Forms.Label LabelXOpacity;
        private System.Windows.Forms.Button ButtonBatchWatermark;
        private System.Windows.Forms.ToolTip ToolTipForTextBoxFilename;
        private System.Windows.Forms.CheckBox CheckBoxApplyXWatermark;
        private System.Windows.Forms.Button ButtonSelectedColor;
        private System.Windows.Forms.NumericUpDown NumUpDownXBrushThickness;
        private System.Windows.Forms.Label LabelXThickness;
    }
}

