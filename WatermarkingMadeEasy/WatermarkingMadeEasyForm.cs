﻿namespace WatermarkingMadeEasy
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Drawing;
    using System.Drawing.Drawing2D;
    using System.Drawing.Imaging;
    using System.IO;
    using System.Linq;
    using System.Windows.Forms;


    /// <summary>
    /// Feature Ideas:
    /// - add a watermark via an image
    /// - add watermark "protection" ie kind of like shutter stock by drawing
    ///   an "X" through the entire picture, then draw the text on top.
    /// - add a watermark to all files in a folder.
    /// </summary>
    public partial class FormMain : Form
    {
        private const string IMAGES_DIR = "Images\\";

        //[System.Runtime.InteropServices.DllImport("gdi32.dll", EntryPoint = "DeleteObject")]
        //public static extern IntPtr DeleteObject(IntPtr hDc);

        private bool IsPictureBoxImageFileInfoValid => pictureBoxImageFileInfo?.Exists == true;

        private Color selectedColor = Color.White; //default

        private bool batchMessageShown;

        private static readonly string[] fontNames = FontFamily.Families.Select(f => f.Name).ToArray();

        /// <summary>
        /// The file info for the picture box
        /// </summary>
        private static FileInfo pictureBoxImageFileInfo;

        public FormMain()
        {
            InitializeComponent();

            PopulateComboBoxes();
        }

        /// <summary>
        /// Populate the comboboxes and set the selection to defaults.
        /// </summary>
        private void PopulateComboBoxes()
        {
            var placements = new List<Tuple<Placement, string>>();

            foreach (Placement placement in Enum.GetValues(typeof(Placement)))
            {
                var placementPair = new Tuple<Placement, string>(placement, placement.ToString().SpaceOutCamelCase());
                placements.Add(placementPair);
            }

            ComboBoxTextLocation.DataSource = placements;

            //Item1 and Item2 because of Tuple
            ComboBoxTextLocation.ValueMember = "Item1";
            ComboBoxTextLocation.DisplayMember = "Item2";

            ComboBoxFonts.Items.AddRange(fontNames);

            //2 to 100, even only, pixel sizes
            ComboBoxFontSize.Items.AddRange
                (
                    Enumerable.Range(2, 100)
                            .Where(i => i % 2 == 0)
                            .Select(i => i.ToString())
                            .ToArray()
                );

            //Defaults:
            ComboBoxTextLocation.SelectedIndex = 4;
            ComboBoxFontSize.SelectedIndex = 10;
            ComboBoxFonts.SelectedIndex = 3;
            ComboBoxFontSize.SelectedIndex = 5;
        }

        /// <summary>
        /// Apply the watermark
        /// </summary>
        private void ApplyWatermark()
        {
            if (!IsPictureBoxImageFileInfoValid)
            {
                MessageBoxer.ShowMessage.Error("You must select an image first!");
                return;
            }

            try
            {
                //OLD COMMETNED CODE KEPT BELOW FOR HISTORICAL PURPOSES
                //DOES NOT LEAK AS MUCH MEMORY AS THE ACTUAL UNCOMMENTED CODE
                //However, the "leaked" memory eventually gets cleaned up via the GC

                Image image = Image.FromFile(pictureBoxImageFileInfo.FullName);

                //Bitmap watermarkedBitmap;

                //using (Image image = Image.FromFile(pictureBoxImageFileInfo.FullName))
                using (Graphics graphics = Graphics.FromImage(image))
                using (WatermarkOptions options = CreateWatermarkOptionsFromUI(image))
                {
                    graphics.SmoothingMode = SmoothingMode.HighQuality;
                    graphics.CompositingQuality = CompositingQuality.HighQuality;

                    Watermark.ApplyWatermark(graphics, options);

                    graphics.Flush();
                    PictureBox.Image?.Dispose();
                    PictureBox.Image = image;

                    //watermarkedBitmap = new Bitmap(image);
                }

                //using GetHbitmap (handle/pointer) as this turned out to be the
                //most efficient? way to not leak memory and convert the graphics
                //to image to bitmap back to image
                //clean up memory. both disposes and the DeleteObject() method
                //are required to not leak any memory. Confirmed with testing.
                //IntPtr hBitmapPtr = watermarkedBitmap.GetHbitmap();

                //PictureBox.Image?.Dispose();

                //PictureBox.Image = Image.FromHbitmap(hBitmapPtr);

                ////DeleteObject is imported via GDI to release the memory
                //DeleteObject(hBitmapPtr);

                //watermarkedBitmap?.Dispose();
            }
            catch (Exception ex)
            {
                TextBoxProgramLog.AppendTextError(ex.Message);
            }
        }

        /// <summary>
        /// Create a new Watermark Options object with choices from the UI
        /// </summary>
        /// <returns></returns>
        private WatermarkOptions CreateWatermarkOptionsFromUI(Image image)
        {
            string text = TextBoxWatermark.Text;
            float fontHeightSelection = Convert.ToInt32(ComboBoxFontSize.SelectedItem);
            string fontSelection = fontNames[ComboBoxFonts.SelectedIndex];
            int opacity = GetOpacity(NumUpDownTextOpacity.Value);
            var font = new Font(fontSelection, fontHeightSelection, GraphicsUnit.Pixel);
            var brush = new SolidBrush(Color.FromArgb(opacity, selectedColor));

            return new WatermarkOptions
            {
                Text = text,
                Font = font,
                Color = selectedColor,
                Brush = brush,
                TextOpacity = (int)NumUpDownTextOpacity.Value,
                Width = image.Width,
                Height = image.Height,
                PaddingX = (int)NumUpDownPaddingX.Value,
                PaddingY = (int)NumUpDownPaddingY.Value,
                Placement = (Placement)ComboBoxTextLocation.SelectedValue,
                XWatermarkOpacity = GetOpacity(NumUpDownXOpacity.Value),
                DrawXWatermark = CheckBoxApplyXWatermark.Checked,
                XWatermarkThickness = (int)NumUpDownXBrushThickness.Value
            };
        }


        /// <summary>
        /// Get the opacity from percent (0 to 100) to a ARGB (uint_8) value aka 0 to 255
        /// </summary>
        /// <returns></returns>
        private int GetOpacity(decimal value)
        {
            return (int)(value / 100 * 255);
        }

        /// <summary>
        /// Save the picturebox image to disk
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ButtonSaveImage_Click(object sender, EventArgs e)
        {
            if (!IsPictureBoxImageFileInfoValid)
            {
                MessageBoxer.ShowMessage.Error("You must select an image first!");
                return;
            }

            SaveImage(PictureBox.Image, pictureBoxImageFileInfo);
        }

        /// <summary>
        /// Save the image
        /// </summary>
        /// <param name="image"></param>
        /// <param name="fileInfoToSave"></param>
        private void SaveImage(Image image, FileInfo fileInfoToSave)
        {
            string newFileName = IMAGES_DIR
                                + DateTime.Now.ToReadableFileString()
                                + "_"
                                + fileInfoToSave.Name;

            //default will be PNG
            ImageFormat format = ImageFormat.Png;

            switch (fileInfoToSave.Extension.ToUpper())
            {
                case ".JPG":
                case ".JPEG":
                    format = ImageFormat.Jpeg;
                    break;
                case ".GIF":
                    format = ImageFormat.Gif;
                    break;
                case ".BMP":
                    format = ImageFormat.Bmp;
                    break;
            }

            image.Save(newFileName, format);

            string message = "Created a watermarked image: "
                + Environment.NewLine
                + Environment.CurrentDirectory
                + @"\"
                + newFileName;

            TextBoxProgramLog.AppendTextLine(message);
        }


        /// <summary>
        /// Update some basic image file information
        /// </summary>
        private void UpdateImageMetaInformation()
        {
            if (IsPictureBoxImageFileInfoValid)
            {
                TextBoxFileName.Text = pictureBoxImageFileInfo.Name;

                LabelImageDimensions.Text = "Dimensions: "
                    + PictureBox.Image.Width + "px W, "
                    + PictureBox.Image.Height + "px H";
            }

            ToolTipForTextBoxFilename.SetToolTip(TextBoxFileName, TextBoxFileName.Text);

            //string fileSizeMB = String.Join("", ((double)fileInfo.Length / 1024 / 1024).ToString().Take(4));

            //LabelFileSize.Text = $"File Size: { fileSizeMB } MB";
        }

        /// <summary>
        /// Select a color to use 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ButtonColorDialog_Click(object sender, EventArgs e)
        {
            var colorDialog = new ColorDialog();

            //set the selected color to the previous chosen color
            colorDialog.Color = selectedColor;

            if (colorDialog.ShowDialog() == DialogResult.OK)
            {
                selectedColor = colorDialog.Color;
                ButtonSelectedColor.BackColor = selectedColor;
            }
        }

        /// <summary>
        /// Select one image
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ButtonSelectImage_Click(object sender, EventArgs e)
        {
            var dialog = new OpenFileDialog();
            dialog.InitialDirectory = ".";
            dialog.Filter = "Images (*.png;*.bmp;*.jpg;*.gif)|*.png;*.bmp;*.jpg;*.jpeg;*.gif| All files (*.*)|*.*";

            if (dialog.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    using (Stream stream = dialog.OpenFile())
                    {
                        if (stream != null)
                        {
                            PictureBox.Image = Image.FromStream(stream);
                        }

                        pictureBoxImageFileInfo = new FileInfo(dialog.FileName);
                        ApplyWatermark();
                    }
                }
                catch (Exception ex)
                {
                    TextBoxProgramLog.AppendTextError(ex.Message);
                    TextBoxProgramLog.AppendTextError(ex.StackTrace);
                }
            }
        }

        /// <summary>
        /// Open the images folder
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ButtonOpenImagesFolder_Click(object sender, EventArgs e)
        {
            try
            {
                string directory = Environment.CurrentDirectory + "\\" + IMAGES_DIR;

                //if directory doesnt exist, create it
                if (!Directory.Exists(directory))
                {
                    Directory.CreateDirectory(directory);
                }

                System.Diagnostics.Process.Start
                    (
                        new System.Diagnostics.ProcessStartInfo()
                        {
                            FileName = directory,
                            UseShellExecute = true,
                            Verb = "open"
                        }
                    );

            }
            catch (Exception ex)
            {
                MessageBoxer.ShowMessage.Error(ex.Message);
            }
        }

        /// <summary>
        /// Do a Batchwatermark of Images
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ButtonBatchWatermark_Click(object sender, EventArgs e)
        {
            //only show message once per program startup
            if (!batchMessageShown)
            {
                MessageBoxer.ShowMessage.Notification("Select multiple entries by clicking on files and by holding down either the Shift button to select many, or CTRL+Click to select one item at a time." +
                    "\r\n\r\n" +
                    "You can use CTRL+A to select all files in the current directory.", "How to use the Batch Watermark feature");

                batchMessageShown = true;
            }

            var dialog = new OpenFileDialog();

            dialog.Multiselect = true;
            dialog.Filter = "Images (*.png;*.bmp;*.jpg;*.gif)|*.png;*.bmp;*.jpg;*.jpeg;*.gif| All files (*.*)|*.*";

            try
            {
                if (dialog.ShowDialog() == DialogResult.OK)
                {
                    var fileInfos = dialog.FileNames
                        .Select(f => new FileInfo(f))
                        .ToArray();

                    foreach (FileInfo batchFileInfo in fileInfos)
                    {
                        BatchWatermarkImages(batchFileInfo);
                    }

                    var notificationDialog = MessageBoxer.ShowMessage.GetUserYesOrNo($"Batch process completed. Processed {fileInfos.Length} images.{Environment.NewLine}Open the Images Folder?");

                    if (notificationDialog == DialogResult.OK)
                    {
                        ButtonOpenImagesFolder_Click(null, null);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        /// <summary>
        /// For batch watermarking images
        /// </summary>
        /// <param name="imageFileInfo"></param>
        private void BatchWatermarkImages(FileInfo imageFileInfo)
        {
            using (Image image = Image.FromFile(imageFileInfo.FullName))
            using (Graphics graphics = Graphics.FromImage(image))
            using (WatermarkOptions options = CreateWatermarkOptionsFromUI(image))
            {
                graphics.SmoothingMode = SmoothingMode.HighQuality;
                graphics.CompositingQuality = CompositingQuality.HighQuality;

                Watermark.ApplyWatermark(graphics, options);

                SaveImage(image, imageFileInfo);
            }
        }




        #region UI Changed events - apply watermark 
        /// <summary>
        /// When the Picturebox is painted (either with new image or new watermark),
        /// log some information for the user.
        /// </summary>
        private void PictureBox_Paint(object sender, PaintEventArgs e)
        {
            if (IsPictureBoxImageFileInfoValid)
            {
                TextBoxProgramLog.AppendTextLine("Image updated.");

                UpdateImageMetaInformation();
            }
        }


        private void ComboBoxFonts_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (IsPictureBoxImageFileInfoValid)
            {
                ApplyWatermark();
            }
        }

        private void TextBoxWatermark_TextChanged(object sender, EventArgs e)
        {
            if (IsPictureBoxImageFileInfoValid)
            {
                ApplyWatermark();
            }
        }

        private void ComboBoxFontSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (IsPictureBoxImageFileInfoValid)
            {
                ApplyWatermark();
            }
        }

        private void ComboBoxTextLocation_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (IsPictureBoxImageFileInfoValid)
            {
                ApplyWatermark();
            }
        }

        private void NumUpDownOpacity_ValueChanged(object sender, EventArgs e)
        {
            if (IsPictureBoxImageFileInfoValid)
            {
                ApplyWatermark();
            }
        }

        private void ButtonApplyWatermarkText_Click(object sender, EventArgs e)
        {
            if (IsPictureBoxImageFileInfoValid)
            {
                ApplyWatermark();
            }
        }

        private void NumUpDownPaddingX_ValueChanged(object sender, EventArgs e)
        {
            if (IsPictureBoxImageFileInfoValid)
            {
                ApplyWatermark();
            }
        }

        private void NumUpDownPaddingY_ValueChanged(object sender, EventArgs e)
        {
            if (IsPictureBoxImageFileInfoValid)
            {
                ApplyWatermark();
            }
        }

        private void NumUpDownXOpacity_ValueChanged(object sender, EventArgs e)
        {
            if (IsPictureBoxImageFileInfoValid && CheckBoxApplyXWatermark.Checked)
            {
                ApplyWatermark();
            }
        }

        private void CheckBoxApplyXWatermark_CheckedChanged(object sender, EventArgs e)
        {
            if (IsPictureBoxImageFileInfoValid)
            {
                ApplyWatermark();
            }
        }

        private void NumUpDownXBrushThickness_ValueChanged(object sender, EventArgs e)
        {
            if (IsPictureBoxImageFileInfoValid && CheckBoxApplyXWatermark.Checked)
            {
                ApplyWatermark();
            }
        }

        #endregion
    }
}
