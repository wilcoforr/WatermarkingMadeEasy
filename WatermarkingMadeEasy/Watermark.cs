﻿namespace WatermarkingMadeEasy
{
    using System;
    using System.Drawing;
    using System.Windows.Forms;

    /// <summary>
    /// Separate "logic" from the UI. 
    /// The UI will prepare choices that will be watermarked onto the graphics object
    /// Then the user can save the changes to disk.
    /// </summary>
    public static class Watermark
    {
        /// <summary>
        /// Add Text to the image
        /// </summary>
        /// <param name="graphics"></param>
        /// <param name="options"></param>
        private static void AddText(Graphics graphics, WatermarkOptions options)
        {
            PointF adjustedLocation = GetPlacementAdjustedPoint(options);

            graphics.DrawString(options.Text, options.Font, options.Brush, adjustedLocation);
        }

        /// <summary>
        /// Overlay an X that goes from one corner to another as further watermark
        /// "protection"
        /// </summary>
        /// <param name="graphics"></param>
        /// <param name="color"></param>
        /// <param name="width"></param>
        /// <param name="height"></param>
        /// <returns></returns>
        private static Graphics OverlayX(Graphics graphics, WatermarkOptions options)
        {
            Color xColor = Color.FromArgb(options.XWatermarkOpacity, options.Color);

            var pen = new Pen(xColor, options.XWatermarkThickness);

            var upperLeft = new PointF(0, 0);
            var bottomRight = new PointF(options.Width, options.Height);

            var upperRight = new PointF(options.Width, 0);
            var bottomLeft = new PointF(0, options.Height);

            graphics.DrawLine(pen, upperLeft, bottomRight);
            graphics.DrawLine(pen, upperRight, bottomLeft);

            return graphics;
        }


        /// <summary>
        /// Adjusted for padding of the watermark text
        /// Also places the placement of the text
        /// </summary>
        /// <param name="options"></param>
        /// <returns></returns>
        private static Point GetPlacementAdjustedPoint(WatermarkOptions options)
        {
            var adjustedLocation = new Point(options.PaddingX, options.PaddingY);

            Size textSize = TextRenderer.MeasureText(options.Text, options.Font);

            //set the location of where to place the text
            switch (options.Placement)
            {
                case Placement.TopLeft:
                    break;
                case Placement.Top:
                    adjustedLocation.X += (options.Width / 2) - (textSize.Width / 2);
                    break;
                case Placement.TopRight:
                    adjustedLocation.X += options.Width - textSize.Width;
                    break;
                case Placement.Right:
                    adjustedLocation.X += options.Width - textSize.Width;
                    adjustedLocation.Y += (options.Height / 2) - (textSize.Height / 2);
                    break;
                case Placement.BottomRight:
                    adjustedLocation.X += options.Width - textSize.Width;
                    adjustedLocation.Y += options.Height - textSize.Height;
                    break;
                case Placement.Bottom:
                    adjustedLocation.X += (options.Width / 2) - (textSize.Width / 2);
                    adjustedLocation.Y += options.Height - textSize.Height;
                    break;
                case Placement.BottomLeft:
                    adjustedLocation.Y += options.Height - textSize.Height;
                    break;
                case Placement.Left:
                    adjustedLocation.Y += (options.Height / 2) - (textSize.Height / 2);
                    break;
                case Placement.Center:
                    adjustedLocation.X += (options.Width / 2) - (textSize.Width / 2);
                    adjustedLocation.Y += (options.Height / 2) - (textSize.Height / 2);
                    break;
            }

            return adjustedLocation;
        }

        public static void ApplyWatermark(Graphics graphics, WatermarkOptions options)
        {
            if (graphics == null)
            {
                throw new ArgumentNullException(nameof(graphics));
            }

            if (options == null)
            {
                throw new ArgumentNullException(nameof(options));
            }

            //draw text onto the image
            if (!String.IsNullOrWhiteSpace(options.Text))
            {
                AddText(graphics, options);
            }

            //apply an X watermark across the whole image
            if (options.DrawXWatermark)
            {
                OverlayX(graphics, options);
            }
        }
    }
}
