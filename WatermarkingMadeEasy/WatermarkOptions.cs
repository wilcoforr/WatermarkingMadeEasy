﻿namespace WatermarkingMadeEasy
{
    using System;
    using System.Drawing;

    public enum Placement
    {
        TopLeft,
        Top,
        TopRight,
        Right,
        BottomRight,
        Bottom,
        BottomLeft,
        Left,
        Center
    }

    /// <summary>
    /// Hold all watermark options to be processed on the graphics object before
    /// being rendered back onto the PictureBox control
    /// </summary>
    public class WatermarkOptions : IDisposable
    {
        /// <summary>
        /// The text to apply to the image
        /// </summary>
        public string Text { get; set; }

        /// <summary>
        /// Opacity (alpha) value for ARGB - note this ranges from 0 to 255
        /// </summary>
        public int TextOpacity { get; set; }

        /// <summary>
        /// Padding in pixels for the X position of the text
        /// </summary>
        public int PaddingX { get; set; }

        /// <summary>
        /// Padding in pixels for the Y position of the text
        /// </summary>
        public int PaddingY { get; set; }

        /// <summary>
        /// Width of the image in pixels
        /// </summary>
        public int Width { get; set; }

        /// <summary>
        /// Height of the image in pixels
        /// </summary>
        public int Height { get; set; }

        /// <summary>
        /// Watermark placement (direction)
        /// </summary>
        public Placement Placement { get; set; }

        /// <summary>
        /// Top, center, bottom right, etc. position where to place text.
        /// Replace with an enum or something later to strongly type this.
        /// </summary>
        public string WatermarkPlacement { get; set; }

        /// <summary>
        /// The selected color for text or the X watermark
        /// </summary>
        public Color Color { get; set; }

        /// <summary>
        /// The Font to use to draw text
        /// </summary>
        public Font Font { get; set; }

        /// <summary>
        /// The brush to use to paint text or shapes/lines
        /// </summary>
        public Brush Brush { get; set; }

        /// <summary>
        /// Draw an X on the image, from corner to corner
        /// </summary>
        public bool DrawXWatermark { get; set; }

        /// <summary>
        /// Opacity in uint8 (0 to 255) for ARGB for drawing the X
        /// </summary>
        public int XWatermarkOpacity { get; set; }

        /// <summary>
        /// Thickness of the brush when drawing the X over the image
        /// </summary>
        public int XWatermarkThickness { get; set; }

        //https://docs.microsoft.com/en-us/dotnet/standard/design-guidelines/dispose-pattern
        //this might be overkill... testing with diagnostics showed that disposing
        //font and brush wasnt really that effective or its a VERY small memory leak
        //that the GC will eventually clean up anyways.
        private bool disposed = false;


        /// <summary>
        /// Dispose and free up Font and Brush resources
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Dispose and free up Font and Brush resources
        /// </summary>
        /// <param name="disposing"></param>
        protected virtual void Dispose(bool disposing)
        {
            if (!disposed && disposing)
            {
                Font?.Dispose();
                Brush?.Dispose();

                disposed = true;
            }
        }
    }
}
