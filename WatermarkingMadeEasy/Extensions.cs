﻿namespace WatermarkingMadeEasy
{
    using System;
    using System.Windows.Forms;

    public static class Extensions
    {
        public static string ToReadableFileString(this DateTime dateTime)
        {
            return dateTime.ToString("MM-dd-yyyy_HH-mm-ss");
        }

        public static void AppendTextLine(this TextBoxBase textBox, string str)
        {
            textBox.AppendText(DateTime.Now.ToShortTimeString() + ": " + str + Environment.NewLine);
        }

        public static void AppendTextError(this TextBoxBase textBox , string str)
        {
            textBox.AppendTextLine(str);
        }

        /// <summary>
        /// Add spaces between letters for a CamelCaseStringLikeThis
        /// https://stackoverflow.com/a/5796427
        /// </summary>
        /// <param name="str">CamelCaseStringLikeThis</param>
        /// <returns>Camel Case String Like This</returns>
        public static string SpaceOutCamelCase(this string str)
        {
            return System.Text.RegularExpressions.Regex.Replace(str, "(\\B[A-Z])", " $1");
        }
    }
}
